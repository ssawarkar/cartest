
public class engine {
	float car_engsize;
	
	//constructor overloading 
	public engine(float engsize)
	{
		car_engsize=engsize;
	}
	
	public float getengsize()
	{
		return car_engsize;
	}
	
	public void setengsize(float engsize)
	{
		car_engsize=engsize;
	}
	
	public void print()
	{
		System.out.println("Size of the car engine is "+getengsize());
	}
}
